import {Box, Grid, Stack} from "@mui/material";


export default function Home() {


    return (
        <>
            <Grid container spacing={2}>
                <Grid item md={6} sm={6} xs={12}>
                    <Box sx={{
                        height: 100,
                        width: '100%',
                        bgcolor: 'red'
                    }}/>
                </Grid>
                <Grid item md={6} sm={6} xs={12}>
                    <Box sx={{
                        height: 100,
                        width: '100%',
                        bgcolor: 'blue'
                    }}/>
                </Grid>

            </Grid>

        </>

    )
}
