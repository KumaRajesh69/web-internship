import { Box, Grid, Stack, Typography } from '@mui/material';
import { Divider } from '@mui/material';
import React from 'react'

const TopPart = () => {
    return(
        <Stack sx={{width: '100%', zIndex: 100}} alignItems={'center'}>
            <Stack direction={'row'} justifyContent={'flex-start'} pl={5} pt={5} >
                <Typography color="white">Home/aboutus</Typography>
            </Stack>
            <Box
                display="flex" flexDirection="column" alignItems="center" justifyContent="center"
                sx={{
                    width: "100%",
                    px: 10,
                    py: 5
                }}>
                <Typography variant="h6" color="white">About Smartkey</Typography>
                <Typography variant="body1" color="white" sx={{ marginTop: "10px" }}>“Leveraging the power of technology to radically alter the functionality of the realty sector.”</Typography>
                <Typography variant="subtitle2" color="white" sx={{ marginTop: "10px" }}>Welcome to a whole new game changing concept called Smart Key. We are an organization engaged in rendering extensive services in the real estate sector leveraging state of the art technology. Smart Key is headquartered in Hyderabad and is a start-up with global ambitions. The new age organization is spear headed by a core team of seasoned professionals drawn from various functional domains within the realm of realty and allied areas.</Typography>
            </Box>
            <Stack sx={{ width: "80%", boxShadow: "0px 4px 9px rgba(0, 0, 0, 0.16)", marginTop: '1', borderRadius: "5px" }}>
                <Stack
                    sx={{
                        width: "100%",
                        margin: "auto",
                        bgcolor: "#E6F7D4",
                        padding: "5px",
                    }}>
                    <Typography variant="body1" textAlign="center" color="#68A820">The firm’s core offerings</Typography>
                </Stack>

                <Stack sx={{
                    margin: "auto",
                    bgcolor: "white",
                    padding: "25px"
                }}>
                    <Grid container spacing={2}>
                        <Grid item xs={6} sx={{ display: "flex", flexDirection: "column", alignItems: "center" }}>
                            <img src="./images/img1.svg" />
                            <Typography variant="body1" mt="5px">Advisory</Typography>
                            <Typography variant="caption" mt="5px" color="#424242" align="center">Comprehensive advisory services through knowledge.</Typography>
                        </Grid>
                        <Grid item xs={6} sx={{ display: "flex", flexDirection: "column", alignItems: "center" }}>
                            <img src="./images/img2.svg" />
                            <Typography variant="body1" mt="5px">Leasing</Typography>
                            <Typography variant="caption" mt="5px" color="#424242" align="center">A technology driven initiative offering leasing services to property owners especially NRIs with assets in India.</Typography>
                        </Grid>
                        <Grid item xs={6} sx={{ display: "flex", flexDirection: "column", alignItems: "center" }}>
                            <img src="./images/img3.svg" />
                            <Typography variant="body1" mt="5px">Management</Typography>
                            <Typography variant="caption" mt="5px" color="#424242" align="center">End to management of realty projects especially from a sales & marketing perspective thus reducing the burden on realtors allowing them to focus on their core operations.</Typography>
                        </Grid>
                        <Grid item xs={6} sx={{ display: "flex", flexDirection: "column", alignItems: "center" }}>
                            <img src="./images/img4.svg" />
                            <Typography variant="body1" mt="5px">Development</Typography>
                            <Typography variant="caption" mt="5px" color="#424242" align="center">Robust in-house capabilities to develop and commission realty projects on a contract/turnkey/BOT basis thus reaching out to realty entrepreneurs aspiring to transition their ideas into reality.</Typography>
                        </Grid>
                    </Grid>
                </Stack>
            </Stack>
            <Stack sx={{
                width: "100%",
                margin: "auto",
                padding: 5
            }}>
                <Grid container spacing={2}>
                    <Grid item xs={6} sx={{ display: "flex", flexDirection: "column", alignItems: "center" }}>
                        <img src="./images/img5.svg" />
                        <Typography variant="h6" mt="5px">Our mission</Typography>
                        <Typography variant="caption" mt="5px" color="#424242">To be recognized as a reliable and trustworthy realty solutions provider acting as a bridge between realtors and end customers; to facilitate consumer empowerment through knowledge dissemination.</Typography>
                    </Grid>
                    <Grid item xs={6} sx={{ display: "flex", flexDirection: "column", alignItems: "center" }}>
                        <img src="./images/img6.svg" />
                        <Typography variant="h6" mt="5px">Our vision</Typography>
                        <Typography variant="caption" mt="5px" color="#424242">To leverage the power of technology and evolve into a massive ecosystem that drives realty transactions, creates knowledge wealth and facilitates seamless interaction between different stakeholders in the real estate domain worldwide.</Typography>
                    </Grid>
                </Grid>
            </Stack>

            <Stack sx={{ bgcolor: "#191919", width: '100%' }}>
                <Box sx={{ display: "flex", pt: "20px", pl: "20px" }}>
                    <Stack direction={'column'} sx={{ width: "100%" }}>
                        <Typography variant="h6" color="white">Address</Typography>
                        <Typography variant="caption" mt="5px" color="#B2B0B0">Unit - 8 , DAV-OUAT Guest House Road,
                            Gopabandhu Nagar,
                            Adjacent to DAV Unit-8,
                            Bhubaneswar - 751008</Typography>
                    </Stack>
                    <Stack direction={'column'} spacing={2} sx={{ width: "100%" }}>
                        <Typography variant="h6" color="white">Company</Typography>
                        <Typography variant="caption" color="#B2B0B0">About Us</Typography>
                        <Typography variant="caption" color="#B2B0B0">Careers</Typography>
                        <Typography variant="caption" color="#B2B0B0">For Partners</Typography>
                        <Typography variant="caption" color="#B2B0B0">Teams</Typography>
                    </Stack>
                    <Stack direction={'column'} spacing={2} sx={{ width: "100%" }}>
                        <Typography variant="h6" color="white">Explore</Typography>
                        <Typography variant="caption" color="#B2B0B0">News</Typography>
                        <Typography variant="caption" color="#B2B0B0">Home Loans</Typography>
                        <Typography variant="caption" color="#B2B0B0">Sitemaps</Typography>
                        <Typography variant="caption" color="#B2B0B0">International</Typography>
                    </Stack>
                    <Stack direction={'column'} spacing={2} sx={{ width: "100%" }}>
                        <Typography variant="h6" color="white">Quick Links</Typography>
                        <Typography variant="caption" color="#B2B0B0">Properties</Typography>
                        <Typography variant="caption" color="#B2B0B0">Builders</Typography>
                        <Typography variant="caption" color="#B2B0B0">Properties</Typography>
                        <Typography variant="caption" color="#B2B0B0">Builders</Typography>
                    </Stack>
                </Box>

                <Divider sx={{ borderWidth: "1px", borderColor: "#424242", mt: "10px", ml: "20px", mr: "20px" }} />
                <Stack direction={'row'} justifyContent={'space-between'} p={3} >
                    <img src="./images/img7.svg" />
                    <Typography variant="caption" color="white">© 2022 All Rights Reserved. Designed by Smartters' Studio</Typography>
                </Stack>

            </Stack >
        </Stack>
    )
}

const AboutUs = () => {
    return (
        <Stack width={'100%'}>
            <Box sx={{width: '100%', position: 'relative'}} >
                <img src={'/images/bg1.svg'} width={'100%'} height={'auto'} alt={'bg image'} style={{position: 'absolute', left: 0, top: 0}}/>
                <Box sx={{position: 'absolute', left: 0, top: 0}}>
                    <TopPart />
                </Box>
            </Box>
        </Stack >

    )
}

export default AboutUs;
