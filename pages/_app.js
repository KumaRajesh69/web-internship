import * as React from 'react';
import PropTypes from 'prop-types';
import Head from 'next/head';
import { ThemeProvider } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import { CacheProvider } from '@emotion/react';
import theme from '../src/theme';
import createEmotionCache from '../src/createEmotionCache';
import MyAppBar from "../src/components/MyAppBar";
import {useRouter} from "next/router";
import {useEffect, useState} from "react";
import { SnackbarProvider } from 'notistack'
import axios from "axios";

// Client-side cache, shared for the whole session of the user in the browser.
const clientSideEmotionCache = createEmotionCache();

export default function MyApp(props) {
    const Router = useRouter();
    // console.log('Router --> ',Router);
    const { Component, emotionCache = clientSideEmotionCache, pageProps } = props;

    const [loading, setLoading] = useState(false);

    useEffect(() => {
        if(loading) return;
        setLoading(true);
        const token = localStorage.getItem('test-token');


        if(token){
            axios.post(
                'https://api.gssgrcsuite.com/authentication',
                {
                    "accessToken": token,
                    "strategy": "jwt"
                })
                .then((response) => {
                    const {data} = response;
                    console.log('Response --> ', data);
                    localStorage.setItem('test-token', data?.accessToken);
                    localStorage.setItem('test-user', JSON.stringify(data?.user));
                    if(Router.pathname === '/login'){
                        Router.push('/').then(() => {
                            setLoading(false);
                        });
                    }else {
                        setLoading(false);
                    }
                })
                .catch((error) => {
                    const data = error?.response?.data
                    console.log('Error --> ', data);
                    localStorage.removeItem('test-token');
                    localStorage.removeItem('test-user');
                    setLoading(false);
                })
        }else {
            if(Router.pathname !== '/login'){
                Router.push('/login').then(() => {
                    setLoading(false);
                });
            }else {
                setLoading(false);
            }
        }
    }, [Router.pathname]);

    return (
        <CacheProvider value={emotionCache}>
            <Head>
                <title>{`${Component.title ? Component.title + ' - ' : ''}Test Project`}</title>
                <meta name="viewport" content="initial-scale=1, width=device-width" />
            </Head>
            <SnackbarProvider anchorOrigin={{
                horizontal: 'center',
                vertical: 'bottom'
            }}>
                <ThemeProvider theme={theme}>
                    {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
                    <CssBaseline />
                    {
                        !Component.hide ?
                        <MyAppBar /> : <></>
                    }
                    {
                        loading ? <></> : <Component {...pageProps} />
                    }
                </ThemeProvider>
            </SnackbarProvider>
        </CacheProvider>
    );
}

MyApp.propTypes = {
    Component: PropTypes.elementType.isRequired,
    emotionCache: PropTypes.object,
    pageProps: PropTypes.object.isRequired,
};