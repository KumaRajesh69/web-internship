import {Button, Grid, Stack, TextField} from "@mui/material";
import {useState} from "react";
import { DesktopDatePicker } from '@mui/x-date-pickers/DesktopDatePicker';
import dayjs from 'dayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import {useRouter} from "next/router";


export default function Home() {

    const [numbers, setNumbers] = useState(new Array(10).fill(0));
    const [value, setValue] = useState(dayjs('2014-08-18T21:11:54'));
    const Router = useRouter();
    const handleChange = (newValue) => {
        setValue(newValue);
    };
    return (
        <Stack p={3}>
            <Grid container spacing={3}>
                {
                    numbers.map((each, pos) => (
                        <Grid item xs={12} sm={6} md={4} lg={3}>
                            <TextField
                                placeholder={`Number ${pos+1}`}
                                type={'number'}
                                value={each}
                                fullWidth
                                sx={{
                                    marginBottom: 2,
                                }}
                                variant="outlined"
                                onChange={(e) => {
                                    setNumbers(() => {
                                        numbers[pos] = parseInt(e.target.value);
                                        return [...numbers];
                                    });
                                }}
                            />
                        </Grid>
                    ))
                }
            </Grid>
            <LocalizationProvider dateAdapter={AdapterDayjs}>
                <DesktopDatePicker
                    placeholder="Date desktop"
                    inputFormat="MM/DD/YYYY"
                    value={value}
                    onChange={handleChange}
                    renderInput={(params) => <TextField {...params} />}
                />
            </LocalizationProvider>
            <Button
                variant={'contained'}
                color={'primary'}
                onClick={() => {
                    let sum = 0;
                    for (const item of numbers) {
                        sum+=item;
                    }
                    console.log('Added --> ',sum)
                }}
            >
                Add
            </Button>
            <Button
                variant={'contained'}
                color={'primary'}
                sx={{mt: 2}}
                onClick={() => {
                    Router.push('/task1');
                }}
            >
                Go To Task 1
            </Button>
            <Button
                variant={'contained'}
                color={'primary'}
                sx={{mt: 2}}
                onClick={() => {
                    Router.replace(
                        '/task2');
                }}
            >
                Go To Task 2
            </Button>
            <Button
                variant={'contained'}
                sx={{mt: 2}}
                color={'primary'}
                onClick={() => {
                    Router.push('/login');
                }}
            >
                Go To Login
            </Button>
        </Stack>
    )
}
