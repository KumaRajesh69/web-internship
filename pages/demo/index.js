import {Box, Button, Grid, Stack, TextField, Typography} from '@mui/material';
import { Divider } from '@mui/material';
import React, {useState} from 'react'
import {useRouter} from "next/router";

const Demo = () => {

    const [data, setData] = useState('');
    const Router = useRouter();

    const handleClick = () => {
        Router.push('/demo/result?val='+data);
    }
    return (
        <Stack width={'100%'} justifyContent={"center"} alignItems={'center'}>
            Demo Page
            <Stack>
              <TextField
                  variant={"outlined"}
                  value={data}
                  onChange={(e) => setData(e.target.value)}
              />
            </Stack>
            <Button sx={{mt:2}} variant={'outlined'} onClick={handleClick}>
                Submit
            </Button>

        </Stack>

    )
}

export default Demo;
