import {Box, Button, Grid, Stack, TextField, Typography} from '@mui/material';
import { Divider } from '@mui/material';
import React, {useEffect, useState} from 'react'
import {useRouter} from "next/router";

const Demo = () => {
    const Router = useRouter();
    const {val} = Router.query;
    const [data, setData] = useState('');

    useEffect(() => {
        setData(val);
    }, [val]);

    return (
        <Stack width={'100%'} justifyContent={"center"} alignItems={'center'}>
            Result Page
            <Stack>
              <TextField
                  variant={"outlined"}
                  value={data}
                  onChange={(e) => setData(e.target.value)}
              />
            </Stack>
            <Button sx={{mt:2}} variant={'outlined'}>
                Submit
            </Button>

        </Stack>

    )
}

export default Demo;
