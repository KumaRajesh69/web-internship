import {Box, Button, Grid, Stack, Typography} from '@mui/material';
import { Divider } from '@mui/material';
import React from 'react'
import {useRouter} from "next/router";

const Demo = () => {
    const Router = useRouter();
    const {demoid, type, qty} = Router.query;
    return (
        <Stack width={'100%'}>
            {type === 'number' ? `Demo Page ${demoid}` : demoid}
            <br />
            {qty * qty}
        </Stack>

    )
}

export default Demo;
