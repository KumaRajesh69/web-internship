import {Button, CircularProgress, Stack, TextField, Typography} from "@mui/material";
import {useState} from "react";
import {useRouter} from "next/router";
import axios from 'axios';
import {useSnackbar} from "notistack";


export default function Home() {

    const [email, setEmail] = useState('');
    const [emailError, setEmailError] = useState('');
    const [password, setPassword] = useState('');
    const [passwordError, setPasswordError] = useState('');
    const [loading, setLoading] = useState(false);
    const Router = useRouter();
    const { enqueueSnackbar } = useSnackbar();

    const validateFields = () => {
        let valid = true;
        if(email === ''){
            setEmailError('Please enter a valid email');
            valid = false;
        }else {
            setEmailError('');
        }
        if(password === '') {
            setPasswordError('Please enter a valid password');
            valid = false;
        }else {
            setPasswordError('');
        }
        return valid;
    }

    const validateFieldsStepByStep = () => {
        if(email === ''){
            setEmailError('Please enter a valid email');
            return false;
        }else {
            setEmailError('');
        }
        if(password === '') {
            setPasswordError('Please enter a valid password');
            return false;
        }else {
            setPasswordError('');
        }
        return true;
    }

    const handleLogin = () => {
        /**
         * Validations
         */
        if(validateFields()){
            setLoading(true);
            /**
             * Todo - API call
             */
            axios.post(
                'https://api.gssgrcsuite.com/authentication',
                {
                    "email": email,
                    "password": password,
                    "strategy": "local"
                })
                .then((response) => {
                    const {data} = response;
                    /**
                     * Do post login things here
                     */
                    enqueueSnackbar('Login Successful', {
                        variant: 'success',
                    });
                    localStorage.setItem('test-token', data?.accessToken);
                    Router.reload();
                })
                .catch((error) => {
                    const data = error?.response?.data
                    enqueueSnackbar(data?.message || 'Something went wrong', {
                        variant: 'error',
                        anchorOrigin: {
                            horizontal: 'right',
                            vertical: 'bottom'
                        }
                    });
                })
                .finally(() => {
                    setLoading(false);
                })
        }

    }
    return (
        <Stack p={3} justifyContent={'center'} alignItems={'center'} height={'100vh'}>
            <Typography sx={{mb: 3}}>
                Login Page
            </Typography>

            <TextField
                placeholder={'Enter your email'}
                type={'email'}
                value={email}
                sx={{mb: 2}}
                error={!!emailError} // '' -> false ! true !! false
                helperText={emailError}
                variant="outlined"
                onChange={(e) => {
                    setEmail(e.target.value);
                }}
            />
            <TextField
                placeholder={'Enter your password'}
                type={'password'}
                value={password}
                sx={{mb: 2}}
                error={!!passwordError}
                helperText={passwordError}
                variant="outlined"
                onChange={(e) => {
                    setPassword(e.target.value);
                }}
            />

            <Button
                variant={'contained'}
                disabled={loading}
                color={'primary'}
                onClick={handleLogin}
            >
                {loading ? <CircularProgress size={18} /> :'Login'}
            </Button>

        </Stack>
    )
}
