import {Button, Stack, Typography, TextField} from "@mui/material";
import Link from "next/link";
import { styled } from '@mui/material/styles';

const CssTextField = styled(TextField)({
    '& label.Mui-focused': {
        color: 'white',
    },
    '& .MuiOutlinedInput-root': {
        color: 'white',
        borderRadius: 8,
        '& fieldset': {
            borderColor: 'white',
        },
        '&:hover fieldset': {
            borderColor: 'white',
        },
        '&.Mui-focused fieldset': {
            borderColor: 'white',
        },
    },
});

const AnotherTextField = styled(TextField)({
    '& .MuiOutlinedInput-root': {
        background: '#F5F5F5',
        borderRadius: 8,
        '& fieldset': {
            borderColor: '#F5F5F5',
        },
    },
});
export default function Task1() {
    return (
        <>
            <Stack p={3} width={'100vw'} bgcolor={'#061C41'}>
                <Stack width={100}>
                    <Button sx={{color: 'white'}} href={'/'}>
                        Demo Link
                    </Button>
                    <Typography sx={{
                        color: 'white',
                        textDecoration: 'none',
                        // '&:hover': {
                        //     textDecoration: 'underline',
                        // }
                    }} component={Link} href={'/'}>
                        Demo Link
                    </Typography>
                    <CssTextField
                        placeholder={'hhhhhh'}
                    />

                </Stack>
            </Stack>
            <AnotherTextField
                color={'primary'}
                placeholder={'hhhhhh'}
            />
        </>
    )
}
Task1.title = 'Task 1';