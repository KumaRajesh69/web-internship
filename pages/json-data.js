import {useEffect, useState} from "react";
import {TextField, Typography} from "@mui/material";

let num3 = 10;
export default function JsonData() {

    //useEffect , useState - React , useRouter - net/router , useTheme - @mui/material/styles
    const [num1, setNum1] = useState('');
    const [num2, setNum2] = useState('');
    const [add, setAdd] = useState('');

    useEffect(() => {
        if(num1)
            setNum2(num1 + 1);
        else
            setNum2('');
    }, [num1]);

    useEffect(() => {
        if(num2)
            num3 = (num2 + 1);
        else
            num3 = ('');
    }, [num2]);


    useEffect(() => {
        if(num1 && num2 && num3)
            setAdd(num1 + num2 + num3);
        else
            setAdd('');
    }, [num1, num2, num3]);


    return(
        <>
            <TextField
                variant={'outlined'}
                label={'Data 1'}
                value={num1}
                onChange={(e) => {
                    setNum1(e.target.value ? parseInt(e.target.value) : '');
                }}
                />
            <TextField
                variant={'outlined'}
                label={'Data 2'}
                value={num2}
                onChange={(e) => setNum2(e.target.value ? parseInt(e.target.value) : '')}
                />

            <Typography>
                {num3}
            </Typography>
            <Typography>
                {add}
            </Typography>
        </>
    )
}
