import React, {useState} from "react";
import {Button, Grid, MenuItem, TextField, Typography} from "@mui/material";
import { styled } from '@mui/material/styles';
import {useRouter} from "next/router";
import MyAppBar from "../../src/components/MyAppBar";
import MyAppBar2 from "../../src/components/MyAppBar2";

const CustomTextField = styled(TextField)({

    '& .MuiOutlinedInput-root': {
        borderRadius: 7,
        height: 48
    },
});
export default function FormTest() {
    const form = [
        {
            label: 'Study Title',
            placeholder: 'Enter Study Title',
            type: 'text',
            key: 'studyTitle',
            gridValue: 12,
        },
        {
            label: 'Study Id',
            placeholder: 'Enter Study Id',
            type: 'text',
            key: 'studyId',
            gridValue: 12,
        },
        {
            label: 'Type of Randomization',
            placeholder: 'Select Randomisation type',
            type: 'select',
            key: 'typeOfRandomisation',
            options: [{label: 'Type1', value: 'type1'},{label: 'Type2', value: 'type2'}],
            gridValue: 6,
        },
        {
            label: 'Site Count',
            placeholder: 'Enter Site Count',
            type: 'text',
            key: 'siteCount',
            gridValue: 6,
        },
    ]
    const Router = useRouter();
    const [data, setData] = useState({});
    const handleChange = (key, value) => {
        let _data = data;
        _data[key] = value;
        setData(_data);
    }
    return(
        <MyAppBar2>
            <Grid container sx={{width: '100%'}}>
                <Grid
                    item
                    container
                    spacing={2}
                    md={6}
                    sm={10}
                    xs={12}
                    sx={{
                        px: {md: 4, sm: 2, xs: 1},
                        py: {md: 4, sm: 2, xs: 2},
                    }}
                >
                    {
                        form.map((each, index) => (
                            <Grid item xs={12} sm={each.gridValue}>
                                <Typography sx={{fontSize: 14}}>
                                    {each.label}
                                </Typography>
                                {
                                    each.type === 'text' ?
                                        <CustomTextField
                                            variant={'outlined'}
                                            onChange={(e) => handleChange(each.key , e.target.value)}
                                            placeholder={each.placeholder}
                                            sx={{mt: 0.6, width: '100%',}}
                                        />:
                                        <CustomTextField
                                            select
                                            defaultValue={"-1"}
                                            onChange={(e) => handleChange(each.key , e.target.value)}
                                            variant={'outlined'}
                                            sx={{mt: 0.6, width: '100%',}}
                                        >
                                            <MenuItem disabled value="-1">
                                                {each.placeholder}
                                            </MenuItem>
                                            {each.options.map((option) => (
                                                <MenuItem key={option.value} value={option.value}>
                                                    {option.label}
                                                </MenuItem>
                                            ))}
                                        </CustomTextField>
                                }
                            </Grid>
                        ))
                    }
                    <Grid xs={12} item>
                        <Button
                            variant={'contained'}
                            onClick={() => {
                                localStorage.setItem('form-value', JSON.stringify(data));
                                Router.push('/form-test/result');
                            }}
                            sx={{width: {sm: 240, xs: '100%'}, mt: 1, height: 44, textTransform: 'capitalize'}}
                        >
                            Add New Study
                        </Button>
                    </Grid>
                </Grid>
            </Grid>

        </MyAppBar2>
    )
}