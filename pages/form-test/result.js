import React, {useEffect, useState} from "react";
import {Box, Stack, Typography} from "@mui/material";

export default function Result() {

    const [data, setData] = useState({key: 1});

    useEffect(() => {
        let _data = localStorage.getItem('form-value');
        setData(_data ? JSON.parse(_data) : {});
        setTimeout(() => {
            localStorage.removeItem('form-value');
        }, 1000);
    }, []);

    return(
        <Stack container sx={{width: '100%'}}>
            {
                Object.keys(data).map((each, pos) => (
                    <Typography>
                        {`${each} : ${data[each]}`}
                    </Typography>

                ))
            }
        </Stack>
    )
}